def RNA_trans():
	inp=raw_input("\nEnter the nucleotide sequence of the dna\n ")
	dna=list(inp)
	nuc=['G','C','T','A']
	for j in dna:
		if(j not in nuc):
			print("invalid Entry\n")	
			exit()
	for i in range(len(dna)):
		if   dna[i]=='G':
			dna[i]='C'
		elif dna[i]=='C':
			dna[i]='G'
		if   dna[i]=='T':
			dna[i]='A'
		elif dna[i]=='A':
			dna[i]='U'
	print(" RNA strand complement:\n ")
	print(dna)

RNA_trans()
